:- encoding('UTF-8').


:- object(board(_Board_)).

    :- info([ version is 1.0
            , author is 'Paul Brown'
            , date is 2019/10/22
            , comment is 'The TicTacToe game board'
            ]).

    :- public(init/0).
    :- mode(init, zero_or_more).
    :- info(init/0,
        [ comment is 'Initialize a new board'
        ]).
    init :-
        % Use numbers for easy human and machine navigation
        _Board_ = [ [1, 2, 3]
                  , [4, 5, 6]
                  , [7, 8, 9]
                  ].

    :- public(available_move/1).
    :- mode(available_move(+integer), zero_or_one).
    :- mode(available_move(-integer), zero_or_more).
    :- info(available_move/1,
        [ comment is 'The number is available for a move to be played'
        , argnames is ['GridNumber']
        ]).
    available_move(N) :-
        list::flatten(_Board_, Flat),
        list::member(N, Flat),
        integer(N).

    :- public(update/3).
    :- mode(update(+integer, +atom, -list), zero_or_one).
    :- info(update/3,
        [ comment is 'Update the board given the move by GridNumber and Player'
        , argnames is ['GridNumber', 'PlayerChar', 'NewBoard']
        ]).
    update(N, C, [N1, R2, R3]) :-
        % In row 1
        _Board_ = [R1, R2, R3],
        list::select(N, R1, C, N1), !.
    update(N, C, [R1, N2, R3]) :-
        % In row 2
        _Board_ = [R1, R2, R3],
        list::select(N, R2, C, N2), !.
    update(N, C, [R1, R2, N3]) :-
        % In row 3
        _Board_ = [R1, R2, R3],
        list::select(N, R3, C, N3), !.

    :- public(print_board/0).
    :- mode(print_board, one).
    :- info(print_board/0,
        [ comment is 'Print the board to stdout'
        ]).
    print_board :-
        _Board_ = [R1, R2, R3],
        write('┌─┬─┬─┐\n'),
        print_row(R1),
        write('├─┼─┼─┤\n'),
        print_row(R2),
        write('├─┼─┼─┤\n'),
        print_row(R3),
        write('└─┴─┴─┘\n').

    % Helper to print board, prints row.
    print_row(Row) :-
        meta::map(print_tile, Row), write('│\n').

    % Helper to print row, prints one tile
    print_tile(Tile) :-
        integer(Tile), format('│~w', Tile)
    ;   Tile == o, write('│○')
    ;   Tile == x, write('│×').

:- end_object.

:- object(player(_C_)).

    :- info([ version is 1.0
            , author is 'Paul Brown'
            , date is 2019/10/22
            , comment is 'A human player'
            ]).

    :- public(has_won/1).
    :- mode(has_won(+list), zero_or_more).
    :- info(has_won/1,
        [ comment is 'Check if the given board is a winning board for the player'
        , argnames is ['Board']
        ]).
    has_won([ [_C_, _C_, _C_]
            , [_, _, _]
            , [_, _, _]
            ]).
    has_won([ [_, _, _]
            , [_C_, _C_, _C_]
            , [_, _, _]
            ]).
    has_won([ [_, _, _]
            , [_, _, _]
            , [_C_, _C_, _C_]
            ]).
    has_won([ [_C_, _, _]
            , [_C_, _, _]
            , [_C_, _, _]
            ]).
    has_won([ [_, _C_, _]
            , [_, _C_, _]
            , [_, _C_, _]
            ]).
    has_won([ [_, _, _C_]
            , [_, _, _C_]
            , [_, _, _C_]
            ]).
    has_won([ [_, _, _C_]
            , [_, _C_, _]
            , [_C_, _, _]
            ]).
    has_won([ [_C_, _, _]
            , [_, _C_, _]
            , [_, _, _C_]
            ]).

    :- public(move/2).
    :- mode(move(+list, -list), zero_or_one).
    :- info(move/2,
       [ comment is 'Ask the human to choose a number and make the move'
       , argnames is ['Board', 'UpdatedBoard']
       ]).
    move(Board, NewBoard) :-
        format('Where should ~w go?~n', _C_),
        read(N), integer(N), integer::between(1, 9, N),
        board(Board)::update(N, _C_, NewBoard)
    ; % Move is invalid, notify and recurse
        write('Can''t make that move\n'),
        move(Board, NewBoard).

:- end_object.

:- object(computer(_C_),
    extends(player(_C_))).

    :- info([ version is 1.0
            , author is 'Paul Brown'
            , date is 2019/10/22
            , comment is 'A computer player'
            ]).

    :- public(move/3).
    :- mode(move(+atom, +list, -list), zero_or_one).
    :- info(move/3,
        [ comment is 'Choose and make a move'
        , argnames is ['Difficulty', 'Board', 'UpdatedBoard']
        ]).
    move(easy, Board, NewBoard) :-
        choose_random_member(N, [1, 2, 3, 4, 5, 6, 7, 8, 9]),
        board(Board)::update(N, _C_, NewBoard),
        format('Computer choooses ~w~n', N).

    move(hard, Board, NewBoard) :-
        choose_move(Board, N),
        board(Board)::update(N, _C_, NewBoard),
        format('Computer choooses ~w~n', N).

    :- private(choose_move/2).
    :- mode(choose_move(+list, -integer), zero_or_one).
    :- info(choose_move/2,
        [ comment is 'Use a strategy to choose a move'
        , argnames is ['Board', 'GridNumber']
        ]).
    choose_move(Board, N) :-
        % Computer can win
        board(Board)::available_move(N),
        board(Board)::update(N, _C_, NewBoard),
        ^^has_won(NewBoard), !.
    choose_move(Board, N) :-
        % Player can win
        board(Board)::available_move(N),
        board(Board)::update(N, x, NewBoard),
        player(x)::has_won(NewBoard), !.
    choose_move(Board, N) :-
        % Pick a corner
        choose_random_member(N, [1, 3, 7, 9]),
        board(Board)::available_move(N), !.
    choose_move(Board, 5) :-
        % Pick the center
        board(Board)::available_move(5), !.
    choose_move(Board, N) :-
        % Pick a middle
        choose_random_member(N, [2, 4, 6, 8]),
        board(Board)::available_move(N), !.

    :- private(choose_random_member/2).
    :- mode(choose_random_member(-any, +list), zero_or_more).
    :- info(choose_random_member/2,
        [ comment is 'Yield elements from list in random order'
        , argnames is ['Elem', 'List']
        ]).
    choose_random_member(N, L) :-
        fast_random::permutation(L, NL),
        list::member(N, NL).


:- end_object.

:- object(game(_Difficulty_)).

    :- info([ version is 1.0
            , author is 'Paul Brown'
            , date is 2019/10/22
            , comment is 'A game of TicTacToe'
            ]).

    :- public(play/0).
    :- mode(play, one).
    :- info(play/0,
        [ comment is 'Play a game of TicTacToe at the game object difficulty'
        ]).
    play :-
        % Invalid difficulty, notify and quit.
        \+ ( _Difficulty_ == easy ; _Difficulty_ == hard ),
        write('You can only play an easy or hard game (game(easy)::play.)\n'), !.
    play :-
        % Valid difficulty, let's play!
        ( _Difficulty_ == easy ; _Difficulty_ == hard ),
        write('Welcome to TicTacToe!\n'),
        board(Board)::init, % Make the board
        turn(x, Board). % Human Player 'x' goes first

    :- private(turn/2).
    :- mode(turn(+atom, +list), zero_or_one).
    :- info(turn/2,
        [ comment is 'Play a turn of the game'
        , argnames is ['PlayerChar', 'Board']
        ]).
    turn(x, Board) :-
        % Human turn
        board(Board)::print_board,
        player(x)::move(Board, NewBoard),
        next_turn(x, NewBoard).
    turn(o, Board) :-
        % Computer turn
        board(Board)::print_board,
        computer(o)::move(_Difficulty_, Board, NewBoard),
        next_turn(o, NewBoard).

    :- private(next_turn/2).
    :- mode(next_turn(+atom, +list), zero_or_one).
    :- info(turn/2,
        [ comment is 'Check for winning or draw, otherwise next turn'
        , argnames is ['PlayerChar', 'NewBoard']
        ]).
    next_turn(C, NewBoard) :-
        ( player(C)::has_won(NewBoard)
        -> board(NewBoard)::print_board,
           format('Player ~w wins!', C)
        ; \+ board(NewBoard)::available_move(_)
        -> board(NewBoard)::print_board,
           write('It''s a draw!')
        ; swap_player(C, NC), turn(NC, NewBoard)
        ).

    :- private(swap_player/2).
    :- mode(swap_player(+atom, -atom), one).
    :- info(swap_player/2,
       [ comment is 'Determine which player char is next'
       , argnames is ['FromPlayer', 'ToPlayer']
       ]).
    swap_player(x, o).
    swap_player(o, x).

:- end_object.
