# TicTacToeTalk

A console TicTacToe game in Logtalk, which you play against the
computer.

## How to play

Launch Logtalk and load the game:

```
?- {loader}.
```

Then you have a choice of difficulties:

```
?- game(easy)::play.
```
or
```
?- game(hard)::play.
```
In an easy game the computer makes random moves. In the hard game it
uses a smart strategy to actively try and win.

## Portability

Due to the use of non-ASCII characters to draw the board, the current
version requires a backend that supports UTF-8 encoding. The game can
currently be played on CxProlog, SICStus Prolog, SWI-Prolog, and YAP.
